// Aaron Maher, ID: 21337918
import java.util.Arrays;

public class Image {

    private int[][] pixels;

    private int width;

    private int height;



    public Image(int[][] pixels) {

        this.pixels = pixels;

        this.height = pixels.length;

        this.width = pixels[0].length;

    }

    // Converts array to string.
    public String toString() {
        return Arrays.deepToString(pixels);
    }

    // If passed a true value, flips the array horizontally.
    // If passed a false value, flips the array vertically.
    public void flip(boolean horizontal) {
        if (horizontal) {
            for (int row = 0; row < height; row++) {
                for (int column = 0; column < (width / 2); column++) {
                    // Find the mirror of the column.
                    int columnMirror = width - 1 - column;

                    // Swap the columns around.
                    int temp = pixels[row][column];
                    pixels[row][column] = pixels[row][columnMirror];
                    pixels[row][columnMirror] = temp;
                }
            }
        } else {
            for (int row = 0; row < (height / 2); row++) {
                // Find the mirror of the row.
                int rowMirror = height - 1 - row;

                // Swap the rows around.
                int[] temp = pixels[row];
                pixels[row] = pixels[rowMirror];
                pixels[rowMirror] = temp;
            }
        }
    }

    // If passed a true value, rotates the array clockwise.
    // If passed a false value, rotates the array counter-clockwise.
    public void rotate(boolean clockwise) {

        int[][] newPixels = new int[width][height];

        if (clockwise) {
                for (int row = 0; row < height; row++) {
                    for (int column = 0; column < width; column++) {
                        newPixels[column][height - 1 - row] = pixels[row][column];
                    }
                }
        } else {
            for (int row = 0; row < height; row++) {
                for (int column = 0; column < width; column++) {
                    newPixels[width - 1 - column][row] = pixels[row][column];
                }
            }
        }

        this.pixels = newPixels;

    }
}