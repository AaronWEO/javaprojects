// Name: Aaron Maher | ID: 21337918
import java.util.ArrayList;
import java.util.Arrays;

public class PlayList {

    String plName;
    ArrayList<Track> plSongs;

    // Create a playlist with a default name.
    public PlayList(){
        plName = "My Playlist";
        plSongs = new ArrayList<>();
    }

    // Create a playlist with a specific name.
    public PlayList(String playListName){
        plName = playListName;
        plSongs = new ArrayList<>();
    }

    // Turns the playlist tracks into strings, adds those strings to a new list -
    // and then returns that list as a string.
    public String toString(){
        ArrayList<String> plString = new ArrayList<>();

        for(Track plSongs : plSongs) {
            plString.add(plSongs.toString());
        }

        return plString.toString();
    }

    // Change the name of a playlist.
    public void setName(String name){
        plName = name;
    }

    // Returns current name of playlist.
    public String getName(){
        return plName;
    }

    // Add a track where only the title and artist are known.
    // The year and duration are set to 0.
    public void add(String title, String artist){
        plSongs.add(new Track(title, artist));
    }

    // Add a track where all data is known.
    public void add(String title, String artist, int year, int duration){
        plSongs.add(new Track(title, artist, year, duration));
    }

    // Add a previously created instance of the Track class to the playlist.
    public void add(Track t){
        plSongs.add(t);
    }

    // Remove a song from the playlist. If title is not present, returns false.
    public boolean remove(String title){
        boolean contains = false;
        for(int i = plSongs.size() - 1; i >= 0; i--) {
            Track track = plSongs.get(i);
            if(track.getTitle().equalsIgnoreCase(title)) {
                plSongs.remove(i);
                contains = true;
            }
        }

        return contains;
    }

    // Displays the tracks in the playlist. Outputs a back-up message if there are none.
    public void showList(){
        if(plSongs.size() == 0) {
            System.out.println("The playlist is empty.");
            return;
        }

        for(Track track : plSongs) {
            System.out.println(track);
        }
    }

    // Displays all tracks in sequential order (if supplied with a false value),
    // or random order (if supplied with a true value).
    public void playAll(boolean random){
        int plSize = plSongs.size();
        int[] plOrder = new int[plSize];
        if(!random) {
            for(int i = 0; i < plSize; i++) {
                plOrder[i] = i;
            }
        } else {
            int[] temp = new int[plSize];
            int plFilled = 0;
            while(plFilled < plSize) {
                int position = (int)(Math.random() * plSize);
                if(temp[position] == 0) {
                    plOrder[plFilled] = position;
                    temp[position] = 1;
                    plFilled++;
                }
            }
        }

        playTracks(plOrder);
    }

    // Displays all tracks by a specified artist.
    public void playOnly(String artist){
        int plSize = plSongs.size();
        int[] plOrder = new int[plSize];
        int plFilled = 0;
        for(int i = 0; i < plSize; i++) {
            Track track = plSongs.get(i);
            if(track.getArtist().toLowerCase().contains(artist.toLowerCase())) {
                plOrder[plFilled] = i;
                plFilled++;
            }
        }

        if(plFilled < plSize) {
            plOrder = Arrays.copyOf(plOrder, plFilled);
        }

        playTracks(plOrder);
    }

    // Displays all tracks by a specified year.
    public void playOnly(int year){
        int plSize = plSongs.size();
        int[] plOrder = new int[plSize];
        int plFilled = 0;
        for(int i = 0; i < plSize; i++) {
            Track track = plSongs.get(i);
            if(track.getYear() == year) {
                plOrder[plFilled] = i;
                plFilled++;
            }
        }

        if(plFilled < plSize) {
            plOrder = Arrays.copyOf(plOrder, plFilled);
        }

        playTracks(plOrder);
    }

    // Displays all tracks in a playlist.
    private void playTracks (int[] plOrder) {
        for(int i : plOrder) {
            System.out.println(plSongs.get(i));
        }
    }
}
