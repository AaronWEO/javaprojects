// Name: Aaron Maher | ID: 21337918
public class PlayListDriver {

    public static void main(String[] args) {

        // Create a new playlist.
        PlayList dopeMusic = new PlayList();

        // Check the name of the playlist.
        System.out.print("[Playlist name:] ");
        System.out.println(dopeMusic.getName());
        System.out.println(); // Line break for display purposes.

        // Default name (cringe). Create a new playlist with a title.
        dopeMusic = new PlayList("Cool Music");

        // Eh, I don't like that name either. Change it again without re-creating.
        dopeMusic.setName("Dope Music");

        // Display new name.
        System.out.print("[Playlist name:] ");
        System.out.println(dopeMusic.getName());
        System.out.println(); // Line break for display purposes.

        // Adding tracks to the playlist.
        dopeMusic.add("Photosynthesis", "Saba", 2016, 202);
        dopeMusic.add("Symmetry", "Saba", 2016, 272);
        dopeMusic.add("Ziplock", "Saba"); // Incomplete information (cringe).
        dopeMusic.add("Bourbon", "Gallant", 2016, 279);
        dopeMusic.add("Jupiter", "Gallant", 2016, 276);
        dopeMusic.add("Bone + Tissue", "Gallant", 2016, 221);
        dopeMusic.add("Still (feat. 6LACK and Smino)", "Saba, 6LACK, Smino", 2022, 226);

        // Display the tracks in the playlist.
        System.out.println("[Playlist tracks:] ");
        dopeMusic.showList();
        System.out.println(); // Line break for display purposes.

        // Remove the cringe incomplete track.
        if(dopeMusic.remove("Ziplock")) {
            System.out.println("Track removed.");
            System.out.println(); // Line break for display purposes.
        }

        // Add it back to the playlist with the correct information.
        dopeMusic.add("Ziplock", "Saba", 2021, 198);

        // Display the tracks as an array.
        System.out.println("[Playlist tracks:] ");
        System.out.println(dopeMusic);
        System.out.println(); // Line break for display purposes.

        // Play all songs in sequential order.
        System.out.println("[Playlist tracks (sequential) :] ");
        dopeMusic.playAll(false);
        System.out.println(); // Line break for display purposes.

        // Play all songs in random order.
        System.out.println("[Playlist tracks (random) :] ");
        dopeMusic.playAll(true);
        System.out.println(); // Line break for display purposes.

        // Play all songs with "Saba" in the artist name.
        System.out.println("[Playlist tracks (by Artist) :] ");
        dopeMusic.playOnly("Saba");
        System.out.println(); // Line break for display purposes.

        // Play all songs from 2016.
        System.out.println("[Playlist tracks: (by year) ] ");
        dopeMusic.playOnly(2016);
        System.out.println(); // Line break for display purposes.

        // Creating a new track. Incomplete information (cringe).
        Track banger = new Track("Life", "Saba");

        // Re-creating the track. Information still incomplete (cringe).
        banger = new Track("LIFE", "Saba", 2018);

        // Re-creating the track again. Information incorrect (super cringe).
        banger = new Track("LIF", "Sava", 2019, 203);

        // Fixing the title.
        if(banger.getTitle().equals("LIF")) {
            banger.setTitle("LIFE");
        }

        // Fixing the artist.
        if(banger.getArtist().equals("Sava")) {
            banger.setArtist("Saba");
        }

        // Fixing the year.
        if(banger.getYear() == 2019) {
            banger.setYear(2018);
        }

        // Fixing the duration.
        if(banger.getDuration() == 203) {
            banger.setDuration(162);
        }

        // Displaying the banger.
        System.out.println(banger);
        System.out.println(); // Line break for display purposes.

        // Adding the banger to my playlist.
        dopeMusic.add(banger);

        // Playing all songs from 2018. Displays the song I just added.
        System.out.println("[Playlist tracks (by year) :] ");
        dopeMusic.playOnly(2018);
        System.out.println(); // Line break for display purposes.

    }
}
