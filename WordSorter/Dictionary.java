// ID: 21337918, Aaron Maher
import java.io.IOException;
import java.util.ArrayList;
import java.io.*;
import java.util.Collections;
import java.util.Scanner;

public class Dictionary {

    private final ArrayList<String> WORDS;
    private final int SHORTEST;
    private final int LONGEST;

    public Dictionary(String filepath, int shortest, int longest) {
        // Constructor to set variables and input words from the file passed as a parameter into the ArrayList.
        WORDS = new ArrayList<>();
        this.SHORTEST = shortest;
        this.LONGEST = longest;

        try {
            var fileData = new File(filepath);
            var data = new Scanner(fileData);

            while(data.hasNextLine()) {
                String line = data.nextLine();
                String[] lineSplit = line.split(",");

                for(String word: lineSplit) {
                    String wordUppercase = trimUp(word);
                    if(wordUppercase.length() >= SHORTEST && wordUppercase.length() <= LONGEST) {
                        if(!inDictionary(wordUppercase)) {
                            WORDS.add(wordUppercase);
                        }
                    }
                }
            }
            Collections.sort(WORDS);
        } catch (IOException e) {
            System.out.println("After careful review and consideration, we have deemed that your Dictionary object is inadequate " +
                    "and does not uphold the standards expected from the Dictionary council. " +
                     "Please try again, entering valid values for the parameters.");
        }
    }

    public boolean add(String word) {
        // Adds a word to the ArrayList if it is not already present.
        String wordUppercase = trimUp(word);
        if(wordUppercase.length() >= SHORTEST && wordUppercase.length() <= LONGEST) {
            if(!inDictionary(wordUppercase)) {
                WORDS.add(wordUppercase);
                Collections.sort(WORDS);
                return true;
            }
        }
        return false;
    }

    public String nextWord() {
        // Selects a random word from the ArrayList. If the ArrayList is empty, returns nothing.
        if(WORDS.size() == 0) {
            return "";
        }
        int randNum = (int)(Math.random() * WORDS.size());
        return WORDS.get(randNum);
    }

    public boolean inDictionary(String word) {
        // Checks if a word is in the ArrayList.
        String wordUppercase = trimUp(word);
        return WORDS.contains(wordUppercase);
    }

    private String trimUp(String word) {
        // Trims and converts a passed String to uppercase.
        String wordUppercase = word.trim().toUpperCase();
        return wordUppercase;
    }

    public String[] toArray() {
        // Converts the ArrayList into a 1D Array for ease of printing.
        int i = WORDS.size();
        return WORDS.toArray(new String[i]);
    }

    public int getSHORTEST() {
        // Allows other classes to access the value of "SHORTEST". (I use this for an error message.)
        return SHORTEST;
    }

    public int getLONGEST() {
        // Allows other classes to access the value of "LONGEST". (I use this for an error message.)
        return LONGEST;
    }
}
