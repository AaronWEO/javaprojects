// ID: 21337918, Aaron Maher
public class DictionaryDriver {
    public static void main (String[] args) {
        // Checks if enough arguments have been passed to create the Dictionary object.
        switch (args.length) {
            case 0:
                System.out.println("Please enter the \"filepath\", \"shortest word length\" and \"longest word length\" parameters.");
                return;
            case 1:
                System.out.println("Please enter the \"shortest word length\" and \"longest word length\" parameters.");
                return;
            case 2:
                System.out.println("Please enter the \"longest word length\" parameter.");
                return;
        }
        // Setting the arguments to variables.
        String filepath = args[0];
        int shortest = Integer.parseInt(args[1]);
        int longest = Integer.parseInt(args[2]);
        // Checking to see if "shortest" is equal to or less than zero. In this scenario, I will not consider it a word.
        // I also check to see whether "longest" is shorter than "shortest" as it could mess up comparisons otherwise.
        if((shortest <= 0) || (longest < shortest)) {
            System.out.println("The value of \"shortest\" must be 1 or more. The value of \"longest\" must be equal to or longer than \"shortest\".");
            return;
        }
        // Creation of the Dictionary object "words".
        var words = new Dictionary(filepath, shortest, longest);
        // A method I created (at the bottom of the driver) to print out the ArrayList in a more orderly way.
        printWords(words);
        // Testing the "nextWord" method. (I placed one up here to see what happens in the instance of an empty ArrayList.)
        System.out.println("Randomly selected word: " + words.nextWord());
        // Creating a "normal" word to try to add to the object.
        String word = "amogus";
        // Attempt to add a word using the method at the bottom of the class.
        addWord(word, words);
        // Print the ArrayList to showcase any changes.
        printWords(words);
        // Creating a word already in "Sample.CSV" to try to add to the object.
        String word2 = "conNectIoN";
        // Attempt to add a word using the method at the bottom of the class.
        addWord(word2, words);
        // Print the ArrayList to showcase any changes.
        printWords(words);
        // Creating an empty word to try to add to the object.
        String word3 = "";
        // Attempt to add a word using the method at the bottom of the class.
        addWord(word3, words);
        // Print the ArrayList to showcase any changes.
        printWords(words);
        // Creating a word with whitespaces to try to add to the object.
        String word4 = "    tRim   ";
        // Attempt to add a word using the method at the bottom of the class.
       addWord(word4, words);
        // Print the ArrayList to showcase any changes.
        printWords(words);
        // Testing the "nextWord" method.
        for(int i = 0; i < 5; i++) {
            System.out.println("Randomly selected word: " + words.nextWord());
        }
    }

    public static void addWord(String word, Dictionary words) {
        // First displays the word to add on the console.
        // It then runs the "inDictionary" method, which checks if a word is in the object already.
        // If the word is not in the object, it attempts to add it using the "add" method.
        // If the "add" method is successful, the program moves on.
        // If the "add" method is not successful due to length, it displays an error message.
        // If the word is already in the object to begin with, it states so on the console.
        System.out.println("Word to add: " + word);
        if(!words.inDictionary(word)) {
            System.out.println("Word not present in dictionary. Attempting to add now...");
            if(!words.add(word)) {
                System.out.println("Oopsie! That word is either too short or too long to be added to this Dictionary object. " +
                        "For this object, words must be between " + words.getSHORTEST() + " and " + words.getLONGEST() +
                        " characters in length.");
            }
        } else {
            System.out.println("Word already present in dictionary.");
        }
    }

    public static void printWords(Dictionary words) {
        // Displays the Dictionary object in rows of 9 words maximum.
        String[] wordString = words.toArray();
        System.out.println();
        for(int i = 0; i < wordString.length; i++) {
            if((i > 0) && ((i % 9) == 0)) {
                System.out.print("\n" + wordString[i] + ", ");
            } else {
                System.out.print(wordString[i] + ", ");
            }
        }
        System.out.println("\n");
    }
}
